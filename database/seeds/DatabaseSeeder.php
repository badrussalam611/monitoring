<?php

namespace Database\Seeders;
namespace Database\Seeder;



use Illuminate\Database\Seeder;
use Jenssegers\Mongodb\Helpers\EloquentBuilder;
use Illuminate\Database\Eloquent\Model;
use UserSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call('UsersTableSeeder');
        $this->call('UserSeeder');
    }
    }
