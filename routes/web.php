<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Route; 
use Illuminate\Support\Facades\Auth;

Route::get('/', 'HomeController@index')->name('dashboard');
Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    /** Campaign Route */
    Route::group(['prefix' => 'campaigns'], function () {
        Route::get('/', 'RouteHandler\CampaignController@index')->name('campaigns');
        Route::get('create', 'RouteHandler\CampaignController@create')->name('createCampaign');
        Route::post('create', 'RouteHandler\CampaignController@store');
        Route::get('report/{id}', 'RouteHandler\CampaignController@viewReport')->name('viewCampaign');
        Route::get('delete/{id}', 'RouteHandler\CampaignController@delete')->name('deleteCampaign');
        Route::get('scrap/{id}', 'RouteHandler\CampaignController@doScrapManual')->name('doScrapCampaign');
        Route::get('{id}', 'RouteHandler\CampaignController@editForm')->name('editCampaign');
        Route::post('{id}', 'RouteHandler\CampaignController@store');
        Route::get('export/{id}', 'RouteHandler\CampaignController@export')->name('exportCampaign');
    });

    /** Sources Route */
    Route::group(['prefix' => 'sources'], function () {
        Route::get('/', 'RouteHandler\SourceController@index')->name('sources');
        Route::get('create', 'RouteHandler\SourceController@create')->name('createSource');
        Route::post('create', 'RouteHandler\SourceController@store');
        Route::get('clone/{id}', 'RouteHandler\SourceController@clone')->name('cloneSource');

        Route::get('{id}', 'RouteHandler\SourceController@editForm')->name('viewSource');
        Route::post('{id}', 'RouteHandler\SourceController@store');
    });

    /** Sources Route */
    Route::group(['prefix' => 'facebook'], function () {
        Route::get('login', 'RouteHandler\SosmedController@fbLogin')->name('fbLogin');
        Route::get('callback', 'RouteHandler\SosmedController@fbCallback')->name('fbCallback');
    });

    /** Sources Route */
    Route::group(['prefix' => 'setting'], function () {
        Route::get('/', 'RouteHandler\SettingController@index')->name('settings');
    });

    /** Testing */
    Route::get('/crawl', 'Module\Scrapping\News@init');
    Route::get('/crawl-tw', 'Module\Scrapping\Twitter@init');
    Route::get('/crawl-fb', 'Module\Scrapping\Facebook@init');
    Route::get('/crawl-ig', 'Module\Scrapping\Instagram@init');
    Route::get('/userTimeline', function() {
        // $tmp = Twitter::getUserTimeline(['screen_name' => 'ridwankamil', 'count' => 20, 'format' => 'json']);
        $tmp = Twitter::getSearch(['q' => '@ridwankamil', 'count' => 100, 'format' => 'json', 'result_type' => 'recent']);
        // $tmp = Twitter::getFollowersIds(['screen_name' => 'ridwankamil']);
        $tmp = json_decode($tmp);
        // $ids_arrays = array_chunk($tmp->ids, 100);
        // $user = Twitter::getUsersLookup(['user_id' => $ids_arrays[0][0]]);
        echo "<pre>";
        print_r($tmp);
        // print_r($ids_arrays[0][0]);
        echo "</pre>";
        return;
    });
    
});