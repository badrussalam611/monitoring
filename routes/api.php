<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['prefix' => 'articles'], function () {
    Route::get('{id?}', 'Module\Api\ApiArticle@index')->name('apiArticle');
    Route::get('{id?}/media', 'Module\Api\ApiArticle@getArticleByMedia')->name('apiArticleByMedia');
    Route::get('{id?}/focus', 'Module\Api\ApiArticle@getArticleByFocus')->name('apiArticleByFocus');
});

Route::group(['prefix' => 'tag'], function () {
    Route::get('{id?}', 'Module\Api\ApiTag@index')->name('apiTagAll');
    Route::get('{id?}/focus', 'Module\Api\ApiTag@index')->name('apiTagByFocus');
});