<?php

return [
    'status' => [
        'draft' => 'Draft',
        'running' => 'Running',
        'finished' => 'Finished',
    ],
    'type' => [
        'news' => 'News Media',
        'socmed' => 'Social Media',
        'google' => 'Google Search',
        'twitter' => 'Twitter',
        'facebook' => 'Facebook',
        'instagram' => 'Instagram',
    ],
    'excludetags' => [
        '', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'yang', 'di', 'dan', 'ini', 'itu', 'dari', 'ke', 'pun', 'txt',
        'tidak', 'ada', 'juga', 'dalam', 'bisa', 'saya', 'akan', 'dengan', 'setelah', 'saat', 'antara', 'dia', 'tak', 'sudah', 'sebagai',
        'menjadi', 'tetap', 'ia', 'karena', 'untuk', 'ya', 'tidak', 'tersebut', 'ujar', 'atau', 'seperti'
    ],
    'token' => [
        'instagram' => env('IG_TOKEN', '3952379405.1677ed0.fec03d46f0744e6d8e70346d79d76597'),
    ],
];
