@extends('layouts.app')

@section('content')
  <div class="p-a lt">
      <div class="row">
          <div class="col-sm-12 text-center">
              <h2 class="mb-0 _300">Settings Sites</h2>
              <small class="text-muted">Beware! Don't edit if you won't!</small>
          </div>
      </div>
  </div>

  <div class="padding">
      <div class="row">
          <div class="col-xs-12">
            <div class="panel panel-default">
              <div class="panel-heading font-bold">Facebook Setup</div>
              <div class="panel-body">
                <div class="form-group">
                  <label>FB Token</label>
                  <input class="form-control" type="text" disabled value="{{ get_option('fb_token') }}">
                  <p>Expired at: <strong>{{ get_option('fb_token_expired') }}</strong></p>
                </div>
                <div class="form-group">
                  <a href="{{ route('fbLogin') }}" class="btn btn-danger">Reset TOKEN</a>
                </div>
              </div>
            </div>
          </div>
      </div>
  </div>
@endsection