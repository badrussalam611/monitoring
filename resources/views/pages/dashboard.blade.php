@extends('layouts.app')

@section('content')
  <div class="p-a lt">
      <div class="row">
          <div class="col-sm-12 text-center">
              <h2 class="mb-0 _300">Welcome to Sentimen</h2>
              <small class="text-muted">Check your <strong>Popularity</strong> On Media</small>
          </div>
      </div>
  </div>
  <div class="padding">

      <div class="row">
          <div class="col-xs-12 col-sm-4">
              <div class="box p-a">
                <div class="pull-left m-r">
                  <span class="w-48 rounded  accent">
                    <i class="material-icons">&#xe151;</i>
                  </span>
                </div>
                <div class="clear">
                  <h4 class="m-0 text-lg _300">{{ num_format($articles->count()) }} <span class="text-sm">News</span></h4>
                  <small class="text-muted">Have scrapped.</small>
                </div>
              </div>
          </div>
          <div class="col-xs-6 col-sm-4">
              <div class="box p-a">
                <div class="pull-left m-r">
                  <span class="w-48 rounded primary">
                    <i class="material-icons">&#xe54f;</i>
                  </span>
                </div>
                <div class="clear">
                  <h4 class="m-0 text-lg _300">{{ num_format($campaigns->count()) }} <span class="text-sm">Projects</span></h4>
                  <small class="text-muted">On running.</small>
                </div>
              </div>
          </div>
          <div class="col-xs-6 col-sm-4">
              <div class="box p-a">
                <div class="pull-left m-r">
                  <span class="w-48 rounded warn">
                    <i class="material-icons">&#xe8d3;</i>
                  </span>
                </div>
                <div class="clear">
                  <h4 class="m-0 text-lg _300">{{ num_format($sources->count()) }} <span class="text-sm">Sources Media</span></h4>
                  <small class="text-muted">News and Social Media.</small>
                </div>
              </div>
          </div>
      </div>

      <div class="row hide">
          <div class="col-sm-12 col-md-12">
              <div class="box">
                  <div class="box-header">
                      <h3>Open Projects <span class="label warning">1</span></h3>
                      <small>Running projects for sentimen</small>
                  </div>
                  <ul class="list inset">
                      <li class="list-item">
                        <a herf class="list-left">
                          <span class="w-40 r-2x _600 text-lg accent">
                              B
                          </apan>
                        </a>
                        <div class="list-body">
                          <div class="m-y-sm pull-right">
                              <a href class="btn btn-xs white">Manage</a>
                          </div>
                          <div><a href>Broadcast web app mockup</a></div>
                          <div class="text-sm">
                              <span class="text-muted"><strong>5</strong> tasks, <strong>3</strong> issues</span> 
                              <span class="label"></span>
                          </div>
                        </div>
                      </li>
                      <li class="list-item">
                        <a herf class="list-left">
                          <span class="w-40 r-2x _600 text-lg success">
                              G
                          </apan>
                        </a>
                        <div class="list-body">
                          <div class="m-y-sm pull-right">
                              <a href class="btn btn-xs white">Manage</a>
                          </div>
                          <div><a href>GoodDesign Bootstrap 4 migration</a></div>
                          <div class="text-sm">
                              <span class="text-muted"><strong>35</strong> tasks, <strong>6</strong> issues</span> 
                              <span class="label"></span>
                          </div>
                        </div>
                      </li>
                      <li class="list-item">
                        <a herf class="list-left">
                          <span class="w-40 r-2x _600 text-lg purple">
                              #
                          </apan>
                        </a>
                        <div class="list-body">
                          <div class="m-y-sm pull-right">
                              <a href class="btn btn-xs white">Manage</a>
                          </div>
                          <div><a href>#Hashtag android app develop</a></div>
                          <div class="text-sm">
                              <span class="text-muted"><strong>52</strong> tasks, <strong>13</strong> issues</span> 
                              <span class="label"></span>
                          </div>
                        </div>
                      </li>
                      <li class="list-item">
                        <a herf class="list-left">
                          <span class="w-40 r-2x _600 blue">
                              <i class="fa fa-lg fa-google"></i>
                          </apan>
                        </a>
                        <div class="list-body">
                          <div class="m-y-sm pull-right">
                              <a href class="btn btn-xs white">Manage</a>
                          </div>
                          <div><a href>Google material design application</a></div>
                          <div class="text-sm">
                              <span class="text-muted"><strong>15</strong> tasks, <strong>3</strong> issues</span> 
                              <span class="label"></span>
                          </div>
                        </div>
                      </li>
                      <li class="list-item">
                        <a herf class="list-left">
                          <span class="w-40 r-2x _600 blue-800">
                              <i class="fa fa-lg fa-facebook"></i>
                          </apan>
                        </a>
                        <div class="list-body">
                          <div class="m-y-sm pull-right">
                              <a href class="btn btn-xs white">Manage</a>
                          </div>
                          <div><a href>Facebook connection web application</a></div>
                          <div class="text-sm">
                              <span class="text-muted"><strong>30</strong> tasks, <strong>5</strong> issues</span> 
                              <span class="label"></span>
                          </div>
                        </div>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
@endsection