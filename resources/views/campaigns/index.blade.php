@extends('layouts.app')

@section('content')
<div class="padding">
    <div class="box">
        <div class="box-header">
            <h2>All Campaigns <a href="{{ route('createCampaign') }}" class="btn btn-fw primary pull-right">Create New</a></h2>
            <br>
        </div>
        <div class="">
            <table class="table table-striped b-t b-b">
                <thead>
                    <tr>
                        <th  style="width:5%">No</th>
                        <th  style="width:25%">Campaign Name</th>
                        <th  style="width:25%">Status</th>
                        <th  style="width:15%">Total Data</th>
                        <th  style="width:15%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i = 1;
                    @endphp
                    @forelse ($campaigns as $campaign)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td><a href="{{ route('viewCampaign', $campaign->_id) }}">{{ $campaign->title }}</a></td>
                            <td>{{ $campaign->status }}</td>
                            <td>{{ $campaign->article->count() }}</td>
                            <td>
                                <div class="btn-group dropdown">
                                    <button class="btn white">Action</button>
                                    <button class="btn white dropdown-toggle" data-toggle="dropdown"></button>
                                    <div class="dropdown-menu pull-right">
                                        <a class="dropdown-item" href="{{ route('viewCampaign', $campaign->_id) }}">View Report</a>
                                        <a class="dropdown-item" href="{{ route('editCampaign', $campaign->_id) }}">Edit</a>
                                        <a class="dropdown-item" href="{{ route('doScrapCampaign', $campaign->_id) }}">Do Scrapping</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="{{ route('exportCampaign', $campaign->_id) }}">Export</a>
                                        <a class="dropdown-item text-danger" href="{{ route('deleteCampaign', $campaign->_id) }}"  onclick="return confirm('Anda yakin akan dihapus? Semua data artikel akan dihapus!')">Delete</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">Belum ada Campaign</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection