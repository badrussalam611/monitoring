@extends('layouts.app')

@section('content')
<style>
  .label.negative {
    background-color: red;
  }
  .label.neutral {
    background-color: #999;
  }
</style>
<div class="padding">
    <div class="margin">
        <h2 class="mb-0 _300">Campaign <strong>{{ $campaign->title }}</strong>! <span class="pull-right"><a class="dropdown-item" href="{{ route('editCampaign', $campaign->_id) }}"><i class="fa fa-cog text-muted"></i></a></span></h2>
        <small class="text-muted">Detail report campaign for {{ $campaign->title }}.</small>
    </div>
    <div class="row">
        <div class="col-md-12 col-xl-4">
            <div class="box">
              <div class="box-header">
                <h3>Score Sentiment Total</h3>
                <small>Calculated all days</small>
              </div>
              <div class="text-center b-t">
                <div class="row-col">
                  <div class="row-cell p-a">
                    <div class="inline m-b">
                      <div ui-jp="easyPieChart" class="easyPieChart" ui-refresh="app.setting.color" data-redraw='true' data-percent="{{ num_format($persent_positive) }}" ui-options="{
                          lineWidth: 8,
                          trackColor: 'rgba(0,0,0,0.05)',
                          barColor: '#0cc2aa',
                          scaleColor: 'transparent',
                          size: 100,
                          scaleLength: 0,
                          animate:{
                            duration: 3000,
                            enabled:true
                          }
                        }">
                        <div>
                          <h5>{{ num_format($persent_positive) }}%</h5>
                        </div>
                      </div>
                    </div>
                    <div>Positive</div>
                  </div>
                  <div class="row-cell p-a">
                    <div class="inline m-b">
                      <div ui-jp="easyPieChart" class="easyPieChart" ui-refresh="app.setting.color" data-redraw='true' data-percent="{{ num_format($persent_negative) }}" ui-options="{
                          lineWidth: 8,
                          trackColor: 'rgba(0,0,0,0.05)',
                          barColor: 'red',
                          scaleColor: 'transparent',
                          size: 100,
                          scaleLength: 0,
                          animate:{
                            duration: 3000,
                            enabled:true
                          }
                        }">
                        <div>
                          <h5>{{ num_format($persent_negative) }}%</h5>
                        </div>
                      </div>
                    </div>
                    <div>Negative</div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="col-md-12 col-xl-4">
            <div class="box">
              <div class="box-header">
                <h3>Source</h3>
                <small>Data grab from several source</small>
              </div><br>
              <ul class="list inset m-0">
                <li class="list-item">
                  <div class="list-left">
                    <span class="w-40 circle red">
                      <i class="fa fa-newspaper-o"></i>
                    </span>
                  </div>
                  <div class="list-body">
                    <div>{{ count($campaign->sources) }} Source Data Media</div>
                    <small class="text-muted text-ellipsis">
                      <strong>{{ num_format($campaign->article->count()) }}</strong> Articles
                    </small>
                  </div>
                </li>
                <li class="list-item">
                  <div class="list-left">
                    <span class="w-40 circle blue">
                      <i class="fa fa-bullhorn"></i>
                    </span>
                  </div>
                  <div class="list-body">
                    <div>Twitter Statistics</div>
                    <small class="text-muted text-ellipsis">
                      <strong>{{ (isset($campaign->twitter_data))? num_format($campaign->twitter_data['followers']):'0' }}</strong> Followers, 
                      <strong>{{ (isset($campaign->twitter_data))? num_format($campaign->twitter_data['friends']):'0' }}</strong> Friends
                    </small>
                  </div>
                </li>
              </ul><br>
            </div>
        </div>
        <div class="col-sm-12 col-md-7 col-lg-8 hidden hide">
            <div class="row no-gutter box dark bg">
                <div class="col-sm-12">
                    <div class="box-header">
                      <h3>Score Perdays</h3>
                      <small>Calculate score perdays</small>
                    </div>
                    <div class="box-body">
                        <div ui-jp="chart" ui-refresh="app.setting.color" ui-options="{
                tooltip : {
                    trigger: 'axis'
                },
                legend: {
                    data:['Positive','Negative','Neutral']
                },
                xAxis : [
                    {
                        type : 'category',
                        boundaryGap : false,
                        data : ['Mon','Tue','Wed','Thu','Fri','Sat','Sun']
                    }
                ],
                yAxis : [
                    {
                        type : 'value'
                    }
                ],
                grid : {
                  x2 : 10
                },
                series : [
                    {
                        name:'Positive',
                        type:'line',
                        stack: 'total',
                        data:[120, 132, 101, 134, 90, 230, 210]
                    },
                    {
                        name:'Negative',
                        type:'line',
                        stack: 'total',
                        data:[220, 182, 191, 234, 290, 330, 310]
                    },
                    {
                        name:'Neutral',
                        type:'line',
                        stack: 'total',
                        data:[150, 232, 201, 154, 190, 330, 410]
                    }
                ]
            }" style="height:360px" >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @php
      $focus = array();
      if (isset($campaign->focus) && $campaign->focus !== null) {
        $focus = preg_split("/\\r\\n|\\r|\\n/", $campaign->focus);
      }
    @endphp
    <div class="row m-t m-b">
      <div class="col-sm-12">
        <hr>
        <div class="margin">
            <h5 class="mb-0 _300"><strong>Statistics</strong> by FOCUS</h5>
            <small class="text-muted">Sentiment based on focus and keywords.</small>
        </div>
      </div>
      @forelse ($focus as $item)
        <div class="col-sm-6">
          <div class="box">
            <div class="box-header">
                <h2>{{ ucfirst($item) }} <small class="total-articles pull-right text-xs">0 Articles</small></h2>
                <small>Focus topic for <strong>"{{ $campaign->keyword }}"</strong>.</small>
            </div>
            <div class="box-body">
              <div ui-jp="plot" ui-refresh="app.setting.color" ui-ajax="true" ui-options="{
                  sAjaxSource: '{{ route('apiArticleByFocus', [ 'id' => $campaign->_id, 's' => $item]) }}'
                }" ui-style="{
                    series: { pie: { show: true, innerRadius: 0, stroke: { width: 0 }, label: { show: true, threshold: 0.05 } } },
                    legend: {backgroundColor: 'transparent'},
                    colors: ['#0cc2aa','#fcc100', '#ccc'],
                    grid: { hoverable: true, clickable: true, borderWidth: 0, color: 'rgba(120,120,120,0.5)' },   
                    tooltip: true,
                    tooltipOpts: { content: '%s: %p.0%' }
                }" style="height:200px"></div>
            </div>
          </div>
        </div>
      @empty
        <div class="text-center">
          <p>Belum ada focus.</p>
        </div>
      @endforelse
    </div>

    <div class="row m-t m-b">
      <div class="col-sm-12">
        <hr>
        <div class="margin">
            <h5 class="mb-0 _300"><strong>Tag Cloud</strong> by FOCUS</h5>
            <small class="text-muted">Tag cloud based on focus and keywords.</small>
        </div>
      </div>
      @forelse ($focus as $item)
        <div class="col-sm-6 col-xs-6">
          <div class="box">
            <div class="box-header">
                <h2>{{ ucfirst($item) }}</h2>
                <small>Tagline by focus for <strong>"{{ $campaign->keyword }}"</strong>.</small>
            </div>
            <div class="box-body">
              <div ui-jp="jQCloud" ui-refresh="app.setting.color" ui-ajax="true" ui-options="{
                  sAjaxSource: '{{ route('apiTagByFocus', [ 'id' => $campaign->_id, 's' => $item]) }}'
                }" style="height:360px"></div>
            </div>
          </div>
        </div>
      @empty
        <div class="text-center">
          <p>Belum ada focus.</p>
        </div>
      @endforelse
    </div>

    <hr>
    <div class="b-b b-primary nav-active-primary">
      <ul class="nav nav-tabs">
        <li class="nav-item">
          <a class="nav-link active" href data-toggle="tab" data-target="#all-articles">All Articles</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href data-toggle="tab" data-target="#by-media">Group by Media</a>
        </li>
      </ul>
    </div>
    <div class="tab-content p-t p-b m-b-md">
      <div class="tab-pane animated fadeIn text-muted active" id="all-articles">
        <div class="box">
            <div class="box-header">
                <h2>All Articles</h2>
            </div>
            <div class="table-responsive">
                <table ui-jp="dataTable" ui-options="{
                  sAjaxSource: '{{ route('apiArticle', $campaign->_id) }}',
                  aoColumns: [
                    { mData: 'no' },
                    { mData: 'title' },
                    { mData: 'source_name' },
                    { mData: 'sentiment' }
                  ]
                }" class="table table-striped b-t b-b">
                    <thead>
                        <tr>
                            <th  style="width:5%">No</th>
                            <th  style="width:60%">Title</th>
                            <th  style="width:10%">Source</th>
                            <th  style="width:15%">Sentiment</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
      </div>
      <div class="tab-pane animated fadeIn text-muted" id="by-media">
        <div class="box">
            <div class="box-header">
                <h2>Statistics by Media</h2>
            </div>
            <div class="table-responsive">
                <table ui-jp="dataTable" ui-options="{
                  sAjaxSource: '{{ route('apiArticleByMedia', $campaign->_id) }}',
                  aoColumns: [
                    { mData: 'no' },
                    { mData: 'title' },
                    { mData: 'total' },
                    { mData: 'positive' },
                    { mData: 'negative' },
                    { mData: 'neutral' },
                    { mData: 'sentiment' }
                  ]
                }" class="table table-striped b-t b-b">
                    <thead>
                        <tr>
                            <th  style="width:5%">No</th>
                            <th  style="width:50%">Source</th>
                            <th  style="width:10%">Articles</th>
                            <th  style="width:5%">Positive</th>
                            <th  style="width:5%">Negative</th>
                            <th  style="width:5%">Neutral</th>
                            <th  style="width:5%">Sentiment</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
      </div>
    </div>
</div>
@endsection