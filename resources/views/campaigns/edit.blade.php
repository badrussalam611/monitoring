@extends('layouts.app')

@section('content')
<div class="padding">
    <div class="row">
        <div class="col-md-6 offset-md-3">
          <div class="box">
            <div class="box-header">
              <h2>Create New Campaign</h2>
              <small>Put detail here for person or brand campaign.</small>
            </div>
            <div class="box-divider m-0"></div>
            <div class="box-body">
                <form action="" method="POST">
                    <div class="form-group">
                        <label for="title">Campaign Name</label>
                        <input type="text" required class="form-control" id="title" placeholder="Write down campaign name here" name="title" value="{{ $campaign->title }}">
                    </div>
                    <div class="form-group">
                        <label for="keyword">Main Keyword</label>
                        <input type="text" required class="form-control" id="keyword" placeholder="Put one main keyword here.." name="keyword" value="{{ $campaign->keyword }}">
                        <p class="help-block" style="font-size: 12px; color: #999; font-style: italic;">Choose person or brand for this main keyword</p>
                    </div>
                    <div class="form-group">
                        <label for="keyword">Focus</label>
                        <textarea name="focus" id="" cols="30" rows="10" class="form-control">{{ isset($campaign->focus)? $campaign->focus:'' }}</textarea>
                        <p class="help-block" style="font-size: 12px; color: #999; font-style: italic;">Put your focus sentiment, 1 keyword per line.</p>
                    </div>
                    <div class="form-group">
                        <label for="keyword">Twitter Account</label>
                        <div class="input-group m-b">
                            <span class="input-group-addon">@</span>
                            <input type="text" class="form-control" placeholder="username" name="twitter" value="{{ $campaign->twitter }}">
                        </div>
                        <p class="help-block" style="font-size: 12px; color: #999; font-style: italic;">Fill this form if person or brand have an account twitter</p>
                    </div>
                    <div class="form-group">
                        <label for="keyword">Instagram Account</label>
                        <div class="input-group m-b">
                            <span class="input-group-addon">@</span>
                            <input type="text" class="form-control" placeholder="username" name="instagram" value="{{ $campaign->instagram }}">
                        </div>
                        <p class="help-block" style="font-size: 12px; color: #999; font-style: italic;">Fill this form if person or brand have an account instagram</p>
                    </div>
                    <div class="form-group">
                        <label for="keyword">Facebook FanPage</label>
                        <div class="input-group m-b">
                            <input type="text" class="form-control" placeholder="FB ID" name="facebook" value="{{ $campaign->facebook }}">
                        </div>
                        <p style="font-size: 12px; color: #999; font-style: italic;">Check FB UID here <a href="https://findmyfbid.com/" target="_blank">https://findmyfbid.com/</a></p>
                    </div>
                    <div class="form-group">
                        <label for="source">Choose Source</label>
                        @foreach ($sources as $item)
                            <div class="col-xs-6">
                                <label class="md-check">
                                    <input type="checkbox" name="sources[]" value="{{ $item->_id }}" {{ (isset($campaign->sources) && in_array($item->_id, $campaign->sources))? 'checked':'' }}>
                                    <i class="indigo"></i>
                                    {{ $item->title }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" id="status" class="form-control input-c">
                            <option value="">-- Select --</option>
                            @foreach (config('sentimen.status') as $key => $value)
                                <option value="{{ $key }}" {{ (isset($campaign->status) && $key == $campaign->status)? 'selected':'' }}>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-fw primary m-b">Save</button>
                    {{ csrf_field() }}
                </form>
            </div>
          </div>
            
        </div>
    </div>
</div>
@endsection