@extends('layouts.app')

@section('content')
<div class="padding">
    <div class="box">
        <div class="box-header">
            <h2>All Sources <a href="{{ route('createSource') }}" class="btn btn-fw primary pull-right">Create New</a></h2>
            <br>
        </div>
        <div class="">
            <table class="table table-striped b-t b-b">
                <thead>
                    <tr>
                        <th  style="width:5%">No</th>
                        <th  style="width:25%">Source Name</th>
                        <th  style="width:10%">Site URL</th>
                        <th  style="width:5%">Type</th>
                        <th  style="width:15%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i = 1;
                    @endphp
                    @forelse ($sources as $source)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td><a href="{{ route('viewSource', $source->_id) }}">{{ $source->title }}</a></td>
                            <td><a href="{{ $source->site }}" target="_blank">{{ $source->site }}</a></td>
                            <td>{{ $source->type }}</td>
                            <td>
                                <div class="btn-group dropdown">
                                    <button class="btn white">Action</button>
                                    <button class="btn white dropdown-toggle" data-toggle="dropdown"></button>
                                    <div class="dropdown-menu pull-right">
                                        <a class="dropdown-item" href="{{ route('viewSource', $source->_id) }}">Edit</a>
                                        <a class="dropdown-item" href="{{ route('cloneSource', $source->_id) }}">Duplicate</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item text-danger">Delete</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">Belum ada Source</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection