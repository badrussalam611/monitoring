@extends('layouts.app')

@section('content')
<div class="padding">
    <div class="row">
        <div class="col-md-6 offset-md-3">
          <div class="box">
            <div class="box-header">
              <h2>Create New Source</h2>
              <small>Put detail here for source news or socmed.</small>
            </div>
            <div class="box-divider m-0"></div>
            <div class="box-body">
                <form action="" method="POST">
                    <div class="form-group">
                        <label for="title">Source Name</label>
                        <input type="text" required class="form-control" id="title" placeholder="Write down source name here" name="title" value="{{ $source->title }}">
                    </div>
                    <div class="form-group">
                        <label for="site">Site URL</label>
                        <input type="text" required class="form-control" id="site" placeholder="http://" name="site" value="{{ $source->site }}">
                    </div>
                    <div class="form-group">
                        <label for="el_url">URL Scrapping</label>
                        <input type="text" required class="form-control" id="el_url" placeholder="" name="el_url" value="{{ $source->el_url }}">
                    </div>
                    <div class="form-group">
                        <label for="el_list">List Element</label>
                        <input type="text" required class="form-control" id="el_list" placeholder="" name="el_list" value="{{ $source->el_list }}">
                    </div>
                    <div class="form-group">
                        <label class="ui-switch m-r">
                            <input type="checkbox" name="el_url_prefix" {{ ($source->el_url_prefix == 'on')? 'checked':'' }} value="on">
                            <i></i>
                        </label>
                        <label for="el_url_prefix">Add Prefix URL Site?</label>
                    </div>
                    <div class="form-group">
                        <label for="el_title">Title Element</label>
                        <input type="text" required class="form-control" id="el_title" placeholder="" name="el_title" value="{{ $source->el_title }}">
                    </div>
                    <div class="form-group">
                        <label for="el_description">Description Element</label>
                        <input type="text" required class="form-control" id="el_description" placeholder="" name="el_description" value="{{ $source->el_description }}">
                    </div>
                    <div class="form-group">
                        <label for="el_date">Date Element</label>
                        <input type="text" required class="form-control" id="el_date" placeholder="" name="el_date" value="{{ $source->el_date }}">
                    </div>
                    <div class="form-group">
                        <label for="type">Type</label>
                        <select name="type" id="type" class="form-control input-c">
                            <option value="">-- Select --</option>
                            @foreach (config('sentimen.type') as $key => $value)
                                <option value="{{ $key }}" {{ (isset($source->type) && $source->type == $key)? 'selected':'' }} >{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-fw primary m-b">Save</button>
                    {{ csrf_field() }}
                </form>
            </div>
          </div>
            
        </div>
    </div>
</div>
@endsection