<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Homepage') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- for ios 7 style, multi-resolution icon of 152x152 -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="{{ asset('assets/images/logo.png') }}">
    <meta name="apple-mobile-web-app-title" content="Sentimen">
    <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" sizes="196x196" href="{{ asset('assets/images/logo.png') }}">

    <!-- style -->
    <link rel="stylesheet" href="{{ asset('assets/animate.css/animate.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/glyphicons/glyphicons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/material-design-icons/material-design-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/styles/app.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/styles/font.css') }}" type="text/css" />

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div class="app" id="app">
        @yield('content')
    </div>

    <script src="{{ asset('assets/libs/jquery/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('assets/libs/jquery/tether/dist/js/tether.min.js') }}"></script>
    <script src="{{ asset('assets/libs/jquery/bootstrap/dist/js/bootstrap.js') }}"></script>
    <script src="{{ asset('assets/libs/jquery/underscore/underscore-min.js') }}"></script>
    <script src="{{ asset('assets/libs/jquery/jQuery-Storage-API/jquery.storageapi.min.js') }}"></script>
    <script src="{{ asset('assets/libs/jquery/PACE/pace.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/config.lazyload.js') }}"></script>
    <script src="{{ asset('assets/scripts/palette.js') }}"></script>
    <script src="{{ asset('assets/scripts/ui-load.js') }}"></script>
    <script src="{{ asset('assets/scripts/ui-jp.js') }}"></script>
    <script src="{{ asset('assets/scripts/ui-include.js') }}"></script>
    <script src="{{ asset('assets/scripts/ui-device.js') }}"></script>
    <script src="{{ asset('assets/scripts/ui-form.js') }}"></script>
    <script src="{{ asset('assets/scripts/ui-nav.js') }}"></script>
    <script src="{{ asset('assets/scripts/ui-screenfull.js') }}"></script>
    <script src="{{ asset('assets/scripts/ui-toggle-class.js') }}"></script>
    <script src="{{ asset('assets/scripts/ui-scroll-to.js') }}"></script>
    <script src="{{ asset('assets/scripts/app.js') }}"></script>
    <script src="{{ asset('assets/libs/jquery/jquery-pjax/jquery.pjax.js') }}"></script>
    <script src="{{ asset('assets/scripts/ajax.js') }}"></script>
</body>
</html>
