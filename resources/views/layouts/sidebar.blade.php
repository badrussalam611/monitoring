<!-- aside -->
<div id="aside" class="app-aside modal nav-dropdown">
  <!-- fluid app aside -->
  <div class="left navside dark dk" data-layout="column">
    <div class="navbar no-radius">
      <!-- brand -->
      <a class="navbar-brand">
          <div ui-include="{{ asset('assets/images/logo.svg') }}"></div>
          <img src="{{ asset('assets/images/logo.png') }}" alt="." class="hide">
          <span class="hidden-folded inline">Sentimen</span>
      </a>
      <!-- / brand -->
    </div>
    <div class="hide-scroll" data-flex>
        <nav class="scroll nav-light">
          
            <ul class="nav" ui-nav>
              <li class="nav-header hidden-folded">
                <small class="text-muted">Main</small>
              </li>
              
              <li>
                <a href="{{ route('dashboard') }}" >
                  <span class="nav-icon">
                    <i class="material-icons">&#xe3fc;
                      <span ui-include="{{ asset('assets/images/i_0.svg') }}"></span>
                    </i>
                  </span>
                  <span class="nav-text">Dashboard</span>
                </a>
              </li>
          
              <li class="nav-header hidden-folded">
                <small class="text-muted">Manages</small>
              </li>
          
              <li>
                <a>
                  <span class="nav-caret">
                    <i class="fa fa-caret-down"></i>
                  </span>
                  <span class="nav-icon">
                    <i class="material-icons">&#xe429;
                      <span ui-include="{{ asset('assets/images/i_4.svg') }}"></span>
                    </i>
                  </span>
                  <span class="nav-text">Campaigns</span>
                </a>
                <ul class="nav-sub nav-mega nav-mega-3">
                  <li>
                    <a href="{{ route('campaigns') }}" >
                      <span class="nav-text">List Campaigns</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{ route('createCampaign') }}" >
                      <span class="nav-text">Create Campaign</span>
                    </a>
                  </li>
                </ul>
              </li>
          
              <li>
                <a>
                  <span class="nav-caret">
                    <i class="fa fa-caret-down"></i>
                  </span>
                  <span class="nav-icon">
                    <i class="material-icons">&#xe429;
                      <span ui-include="{{ asset('assets/images/i_4.svg') }}"></span>
                    </i>
                  </span>
                  <span class="nav-text">Sources</span>
                </a>
                <ul class="nav-sub nav-mega nav-mega-3">
                  <li>
                    <a href="{{ route('sources') }}" >
                      <span class="nav-text">List Sources</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{ route('createSource') }}" >
                      <span class="nav-text">Create Source</span>
                    </a>
                  </li>
                </ul>
              </li>

              <li><hr></li>
          
              <li>
                <a>
                  <span class="nav-caret">
                    <i class="fa fa-caret-down"></i>
                  </span>
                  <span class="nav-icon">
                    <i class="material-icons">&#xe429;
                      <span ui-include="{{ asset('assets/images/i_4.svg') }}"></span>
                    </i>
                  </span>
                  <span class="nav-text">Settings</span>
                </a>
                <ul class="nav-sub nav-mega nav-mega-3">
                  <li>
                    <a href="{{ route('settings') }}" >
                      <span class="nav-text">General</span>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
        </nav>
    </div>
  </div>
</div>
<!-- / -->