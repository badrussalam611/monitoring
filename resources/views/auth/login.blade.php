@extends('layouts.app-blank')

@section('content')
  <div class="center-block w-xxl w-auto-xs p-y-md">
    <div class="navbar">
      <div class="pull-center">
        <div ui-include="'../views/blocks/navbar.brand.html'"></div>
      </div>
    </div>
    <div class="p-a-md box-color r box-shadow-z1 text-color m-a">
      <div class="m-b text-sm">
        Sign in with your Sentimen Account
      </div>
      <form name="form" action="" method="POST">
        <div class="md-form-group float-label{{ $errors->has('email') ? ' has-error' : '' }}">
          <input type="email" class="md-input" name="email" value="{{ old('email') }}" required autofocus>
          <label>Email</label>
            @if ($errors->has('email'))
                <span class="text-danger">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <div class="md-form-group float-label{{ $errors->has('password') ? ' has-error' : '' }}">
          <input type="password" class="md-input" name="password" required>
          <label>Password</label>
            @if ($errors->has('password'))
                <span class="text-danger">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>      
        <div class="m-b-md">        
          <label class="md-check">
            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}><i class="primary"></i> Keep me signed in
          </label>
        </div>
        {{ csrf_field() }}
        <button type="submit" class="btn primary btn-block p-x-md">Sign in</button>
      </form>
    </div>
  </div>
@endsection
