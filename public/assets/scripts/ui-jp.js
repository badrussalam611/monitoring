(function ($, MODULE_CONFIG) {
  	"use strict";
  
	$.fn.uiJp = function(){

		var lists  = this;

        lists.each(function()
        {
        	var self = $(this);
			var options = eval('[' + self.attr('ui-options') + ']');
            if (self.attr('ui-ajax') == 'true' && self.attr('ui-jp') == 'plot') {
                $.ajax({
                    url: options[0].sAjaxSource,
                    type: "GET",
                    success: function(res) {
                        options = eval("[" + res.chart + "," + self.attr('ui-style') + "]");
                        self.parent().parent().find('.total-articles').html(res.data.total + ' Articles');
                        if ($.isPlainObject(options[0])) {
                            options[0] = $.extend({}, options[0]);
                        }
                        uiLoad.load(MODULE_CONFIG[self.attr('ui-jp')]).then( function(){
                            self[self.attr('ui-jp')].apply(self, options);
                        });
                    }
                });
            } else if (self.attr('ui-jp') == 'jQCloud') {
                $.ajax({
                    url: options[0].sAjaxSource,
                    type: "GET",
                    success: function(res) {
                        options = eval(res.data);
                        uiLoad.load(MODULE_CONFIG[self.attr('ui-jp')]).then( function(){
                            self.jQCloud(options, {
                                classPattern: null,
                                colors: ["#800026", "#bd0026", "#e31a1c", "#fc4e2a", "#fd8d3c", "#feb24c"],
                                fontSize: {
                                    from: 0.07,
                                    to: 0.02
                                }
                            });
                        });
                    }
                });
            } else {
    			if ($.isPlainObject(options[0])) {
    				options[0] = $.extend({}, options[0]);
    			}

    			uiLoad.load(MODULE_CONFIG[self.attr('ui-jp')]).then( function(){
    				self[self.attr('ui-jp')].apply(self, options);
    			});
            }
        });

        return lists;
	}

})(jQuery, MODULE_CONFIG);
