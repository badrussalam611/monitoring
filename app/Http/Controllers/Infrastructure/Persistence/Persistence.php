<?php
namespace App\Http\Controllers\Infrastructure\Persistence;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Schema;

/**
 * Class Persistence
 *
 * @package App\Http\Controllers\Infrastructure\Persistence
 */
class Persistence
{
    protected $object;
    protected $table;

    public $upload_dir = 'uploads/images';

    /**
     * Find Model By id
     * @param $id       Value ID Record
     * @param $model    Eloquent Model Object
     *
     * @return mixed
     */
    public function find($id, $model)
    {
        $this->object = $model::find(intval($id));
        return $this->object;
    }

    /**
     * Find Model By ID With Eager Loading
     * @param       $id
     * @param       $model
     * @param array $with
     */
    public function findWith($id, $model, $with = [])
    {
        $this->object = $model::find(intval($id))->with($with)->where('id', $id);
        return $this->object->first();
    }

    /**
     * Get All Record
     * @param $model    Eloquent Model Object
     *
     * @return mixed
     */
    public function findAll($model)
    {
        $this->object = $model::all();
        return $this->object;
    }

    /**
     * Search First User or Create New One
     * @param $model
     * @param $args
     *
     * @return mixed
     */
    public function firstOrCreate($model, $args)
    {
        $this->object = $model::firstOrCreate($args);
        return $this->object;
    }

    /**
     * Find Record By Single Columns
     * @param        $value     Search Value
     * @param        $model     Eloquent Model Object
     * @param        $column    Column Name
     * @param string $ops       Where Operation : =, <=, >=, >, <, !=  (default : =)
     *
     * @return mixed
     */
    public function findByColumn($value, $model, $column, $ops = '=')
    {
        $this->object = $model::Where($column, $ops, $value);
        $this->object = $this->object->get();
        return $this->object;
    }

    /**
     * Find Record by Multiple Columns
     * @param $model    Eloquent Model Object
     * @param $args     Array of Argument [column, ops, value]
     *
     * @return mixed
     */
    public function findByMultipleColumn($model, $args)
    {
        $this->object = $model::Where($args[0][0], $args[0][1], $args[0][2]);
        if(count($args) > 0) {
            for($i = 1; $i < count($args); $i++)  {
                $this->object = $this->object->where($args[$i][0], $args[$i][1], $args[$i][2]);
            }
        }

        if($this->object->count() == 1) {
            $this->object = $this->object->first();
        } else {
            $this->object = $this->object->get();
        }

        return $this->object;
    }

    /**
     * Get record by clauses and value
     * @param $model   Search Value
     * @param $where   Parameter of where query
     * @param $classes Parameter for clauses class eloquent
     * 
     * @return mixed
     */
    public function findByClauses($model, $where, $classes = array())
    {
        if (count($where) > 0) {
            $this->object = $model::Where($where[0], $where[1], $where[2]);
        }

        if (count($classes) > 0) {
            foreach ($classes as $key => $value) {
                if ($value != '')
                    $this->object = $this->object->$key($value);
                else
                    $this->object = $this->object->$key();
            }
        }

        if($this->object->count() == 1) {
            $this->object = $this->object->first();
        } else {
            $this->object = $this->object->get();
        }

        return $this->object;
        
    }

    /**
     * Find By Single Column, Then Return First Record
     * @param $value    Search Value
     * @param $model    Eloquent Model Object
     * @param $column   Column
     *
     * @return mixed
     */
    public function findByColumnAndLastOne($value, $model, $column)
    {
        $this->object = $model::Where($column, $value)->orderBy('created_at', 'desc')->take(1);
        $this->object = $this->object->first();
        return $this->object;
    }

    /**
     * Prepare Eloquent Model Object Attributes
     * @param $array    Prepare Model Values
     * @param $object   Eloquent Model Object
     *
     * @return mixed
     */
    public function prepare($array, $object = null)
    {
        if($object == null) {
            $object = $this->object;
        }

        $columns = Schema::getColumnListing( $object->getTable() );
        foreach ($array as $key => $value) {
            if( in_array($key, $columns) ) {
                $object->$key = $value;

                /** Save File, Then Return Path */
                if($value instanceof UploadedFile) {
                    $filename = str_random(10) . '.' . $value->extension();
                    $value->move($this->upload_dir, $filename);
                    $object->$key = $this->upload_dir . '/' .$filename;
                }
            }

        }

        $this->object = $object;
        return $object;
    }

    /**
     * Prepare Eloquent Model Object Attributes
     * @param $array    Prepare Model Values
     * @param $object   Eloquent Model Object
     *
     * @return mixed
     */
    public function prepareFlat($array, $object = null)
    {
        if($object == null) {
            $object = $this->object;
        }

        foreach ($array as $key => $value) {
            $object->$key = $value;

            /** Save File, Then Return Path */
            if($value instanceof UploadedFile) {
                $filename = str_random(10) . '.' . $value->extension();
                $value->move($this->upload_dir, $filename);
                $object->$key = $this->upload_dir . '/' .$filename;
            }
        }

        $this->object = $object;
        return $object;
    }

    /**
     * Persisting Object
     * @param null $object  Eloquent Model Object
     *
     * @return mixed
     */
    public function save($object = null)
    {
        if($object == null) {
            $this->object->save();
        } else {
            $this->object = $object->save();
        }
        return $this->object;
    }


    /**
     * Get Has Many Object Relation
     * @param $entity   Eloquent Model Name
     * @param $objects  Eloquent Model Object
     *
     * @return mixed
     */
    public function hasMany($entity, $objects)
    {
        $this->object->$entity()->saveMany($objects);
        return $this->object;
    }

    /**
     * Count All Record
     * @param null $object  Eloquent Model Object
     *
     * @return mixed
     */
    public function count($object = null)
    {
        if($object == null) {
            $object = $this->object;
        }
        return $object->count();
    }

    /**
     * Get Multiple Where Using Multiple Connection
     * @param       $connection
     * @param       $table
     * @param array $clause
     * @param bool  $first
     *
     * @return mixed
     */
    public function ConnectMultipleWhereLastOne($connection, $table, $clause = [], $first = true)
    {
        $builder = DB::connection( $connection )
            ->table( $table );

        foreach ($clause as $item) {
            $builder->where($item[0], $item[1], $item[2]);
        }
        if($first) {
            return $builder->first();
        }

        return $builder->get();
    }

    /**
     * Flush Active Object
     */
    public function flush()
    {
        $this->object = null;
    }
}
