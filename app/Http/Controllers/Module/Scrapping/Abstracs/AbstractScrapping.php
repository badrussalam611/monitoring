<?php

namespace App\Http\Controllers\Module\Scrapping\Abstracts;

abstract class AbstractScrapping
{
    public $id;
    public $type;
    public $model;
    public $source;
    public $sentiment;
    public $score;
    public $url;
}