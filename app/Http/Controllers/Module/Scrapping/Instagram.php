<?php

namespace App\Http\Controllers\Module\Scrapping;

use Antoineaugusti\LaravelSentimentAnalysis\SentimentAnalysis;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Infrastructure\Persistence\Persistence;
use App\Model\Article;
use App\Model\Campaign;
use App\Model\Post;
use App\Model\Source;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
\Unirest\Request::verifyPeer(false);
use InstagramScraper\Instagram as IG;

class Instagram extends Controller
{
    protected $source;
    protected $campaign;
    protected $sentiment;
    protected $limit = 100;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct($limit = null)
    {
        $this->sentiment = new SentimentAnalysis();
        if ($limit !== null) {
            $this->limit = $limit;
        }
    }

    public function init()
    {
        $campaigns = Campaign::where('status', '=', 'running')->get();
        foreach ($campaigns as $campaign) {
            \Log::info("========================================");
            \Log::info("Start Campaign Instagram: $campaign->title");
            $this->initSource($campaign);
            \Log::info("End of Campaign Instagram: $campaign->title");
            \Log::info("========================================");
        }
    }

    public function initSource($campaign)
    {
        $this->campaign = $campaign;
        foreach ($campaign->sources as $source) {
            $this->source = Source::where('_id', '=', $source)->first();
            if ($this->source && in_array($this->source->type, ['instagram'])) {
                if (isset($this->campaign->instagram) && $this->campaign->instagram !== '') {
                    \Log::info("Start Instagram Get Posts '{$this->campaign->instagram}': ");
                    $this->getPosts( $this->campaign->instagram );
                    \Log::info("Start Instagram Get Comment Post '{$this->campaign->instagram}': ");
                    $this->getPostComments( $this->campaign->_id );
                    \Log::info("End of Instagram Get Posts: ");
                }
            }
        }
    }

    public function getPosts($username)
    {
        /** GET user medias */
        try {
            $medias = IG::getMedias($username, $this->limit);
        } catch (Exception $e) {
            \Log::error($e->getResponse());
        }

        foreach ($medias as $post) {
            $check_post = Post::where('post_id', '=', $post->id)->first();
            if ($check_post == null) {
                \Log::info("Instagram POST URL: {$post->link}");
                $payload = [
                    'post_id' => $post->id,
                    'url' => $post->link,
                    'message' => (!empty($post->caption))? trim_content($post->caption):'',
                    'media' => (isset($post->videoStandardResolutionUrl))? $post->videoStandardResolutionUrl:$post->imageHighResolutionUrl,
                    'date' => date('Y-m-d', $post->createdTime),
                    'source' => 'instagram',
                    'user_id' => $post->ownerId,
                    'campaign_id' => $this->campaign->_id,
                    'likes' => $post->likesCount,
                    'comments' => $post->commentsCount,
                ];
                $persist = new Persistence();
                $persist->prepareFlat( $payload, new Post)->save();
            } else {
                $check_post->likes = $post->likesCount;
                $check_post->comments = $post->commentsCount;
                $check_post->save();
            }
        }
    }

    public function getPostComments($campaign_id)
    {
        $posts = Post::where('campaign_id', '=', $campaign_id)
            ->where('source', '=', 'instagram')
            ->get();
        $ig_comments = [];

        try {
            $ig = IG::withCredentials('orgbjr', 'Kelincix87', config('session.files'));
            $ig->login();
        } catch (Exception $e) {
            \Log::error($e->getResponse());
        }

        foreach ($posts as $post) {
            try {
                $ig_comments = $ig->getMediaCommentsById($post['post_id'], $this->limit);
            } catch (Exception $e) {
                \Log::error($e->getResponse());
            }

            foreach ($ig_comments as $comment) {
                $content = (!empty($comment->text))? $comment->text:'';
                $content_trim = trim_content($content);
                $detail_url = "https://www.instagram.com/{$comment->id}";

                $check_post = Article::where('url', '=', $detail_url)
                    ->where('campaign_id', '=', $this->campaign->_id)
                    ->first();

                if ($check_post == null) {
                    \Log::info("Instagram POST '{$post['post_id']}' Comment User: {$comment->owner->username}");
                    $payload = [
                        'url' => $detail_url,
                        'campaign_id' => $this->campaign->_id,
                        'title' => $content_trim,
                        'content' => $content,
                        'scores' => $this->sentiment->scores($content_trim),
                        'sentiment' => $this->sentiment->decision($content_trim),
                        'score' => $this->sentiment->score($content_trim),
                        'words' => $this->sentiment->words($content_trim),
                        'date' => date('Y-m-d', $comment->createdAt),
                        'source' => $this->source->_id,
                        'source_name' => $this->source->title,
                        'post_id' => $post->post_id,
                        'instagram' => [
                            'user_id' => $comment->owner->id,
                            'user_name' => $comment->owner->username,
                            'user_fullname' => $comment->owner->fullName,
                            'user_biography' => $comment->owner->biography,
                            'followers' => $comment->owner->followedByCount,
                            'following' => $comment->owner->followsCount,
                            'url' => $post->url,
                        ]
                    ];
                    $persist = new Persistence();
                    $persist->prepareFlat( $payload, new Article)->save();
                }
            }
        }

    }
}