<?php

namespace App\Http\Controllers\Module\Scrapping;

use App\Model\Source;
use App\Model\Article;
use App\Model\Campaign;
use App\Jobs\ScrapFirstData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Antoineaugusti\LaravelSentimentAnalysis\SentimentAnalysis;
use App\Http\Controllers\Infrastructure\Persistence\Persistence;

class News extends Controller
{
    protected $source;
    protected $max_page = 5;
    protected $campaign;
    protected $sentiment;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct($max_page = null)
    {
        $this->sentiment = new SentimentAnalysis(resource_path('data/PHPInsight/data/'));
        if ($max_page !== null) {
            $this->max_page = $max_page;
        }
    }

    public function init()
    {
        $campaigns = Campaign::where('status', '=', 'running')->get();
        foreach ($campaigns as $campaign) {
            \Log::info("Schedule Campaign: {$campaign->title} Start!");
            dispatch( (new ScrapFirstData($campaign, 5)) );
        }
    }

    public function initSource($campaign, $source = null)
    {
        $this->campaign = $campaign;

        try {

            if (!is_null($source)) {
                $this->source = Source::where('_id', '=', $source)->first();

                if ($this->campaign->isInqueue($this->source->_id)) {
                    \Log::info("News Source: {$this->source->title} still in queue!");
                    return false;
                } else {
                    $this->campaign->setActivity($this->source->_id);
                }

                \Log::info("Start News Source: {$this->source->title} - {$this->source->type}");
                if ($this->source && in_array($this->source->type, ['news'])) {
                    // $this->max_page = 2;
                    $this->getDataNews();
                } elseif ($this->source && in_array($this->source->type, ['google'])) {
                    $this->max_page = 5;
                    $this->getDataGoogle();
                }
                \Log::info("ENDED News Source: " . $this->source->title);
                $this->campaign->setActivity($this->source->_id, 'finished');

            } else {
                // foreach ($campaign->sources as $source) {
                //     $this->source = Source::where('_id', '=', $source)->first();

                //     \Log::info("Start News Source: " . $this->source->title);
                //     if ($this->source && in_array($this->source->type, ['news'])) {
                //         // $this->max_page = 2;
                //         $this->getDataNews();
                //     } elseif ($this->source && in_array($this->source->type, ['google'])) {
                //         $this->max_page = 5;
                //         $this->getDataGoogle();
                //     }
                //     \Log::info("ENDED Google Source: " . $this->source->title);
                // }
            }

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return $e->getMessage();
        }
    }

    public function getDataNews()
    {
        \Log::info("Scrapping source: {$this->source->title}");
        for ($i=0; $i <= $this->max_page; $i++) {


            try {
                switch ($this->source->site) {
                    case 'https://republika.co.id/':
                        $url_pages = sprintf($this->source->el_url, $this->campaign->keyword);
                        $client = new \Goutte\Client();
                        $crawler = $client->request('POST', $url_pages, [
                            'datestart' => '',
                            'dateend' => '',
                            'q' => $this->campaign->keyword,
                            'sort-type' => 'terbaru',
                            'offset' => $i * 20,
                        ]);
                        break;

                    case 'https://www.okezone.com/':
                        $url_pages = sprintf($this->source->el_url, $this->campaign->keyword, $i * 16);
                        $client = new \Goutte\Client();
                        $crawler = $client->request('GET', $url_pages);
                        break;

                    case 'https://www.viva.co.id/':
                        $client = new \Goutte\Client();
                        $url_get_token = sprintf($this->source->el_url, $this->campaign->keyword);
                        $crawler_token = $client->request('GET', $url_get_token);
                        $token = get_string_between( $crawler_token->text(), 'window.csrf="', '";' );
                        $url_pages = 'https://www.viva.co.id/request/loadmoresearch?page=' . $i;

                        $crawler = $client->request('POST', 'https://www.viva.co.id/request/loadmoresearch', [
                            'keyword'   => $this->campaign->keyword,
                            'ctype'     => 'art',
                            'page'      => $i,
                            '_token'    => $token,
                        ]);
                        break;
                    
                    default:
                        $url_pages = sprintf($this->source->el_url, $this->campaign->keyword, $i);
                        $client = new \Goutte\Client();
                        $crawler = $client->request('GET', $url_pages);
                        break;
                }

                \Log::info("Scrap URL: {$url_pages} Page: {$i}");
                $posts = $crawler->filter($this->source->el_list);

                $posts->each(function($article) use ($client) {
                    if ($article->count() > 0) {
                        if ($this->source->el_url_prefix == 'on') {
                            $detail_url = $this->source->site . $article->filter("a")->attr("href");
                        } else {
                            $detail_url = $article->filter("a")->attr("href");
                        }

                        switch ($this->source->site) {
                            case 'https://republika.co.id/':
                                if (strpos($detail_url, 'video.') !== false) {
                                    continue;
                                }
                                break;
                        }

                        DB::beginTransaction();

                        $check_post = Article::where('url', '=', $detail_url)
                            ->where('campaign_id', '=', $this->campaign->_id)
                            ->first();

                        if ($check_post == null) {
                            $get_article = $client->request('GET', $detail_url );
                            $title = $get_article->filter($this->source->el_title);
                            $content = $get_article->filter($this->source->el_description);
                            $date = $get_article->filter($this->source->el_date);
                            if ($title->count() > 0 && $content->count() > 0 && $date->count() > 0) {
                                \Log::info("#{$this->campaign->_id}, {$this->source->title}: {$detail_url}");
                                $title = $title->text();
                                $content_origin = $content->text();
                                $content = trim_content($content_origin);
                                $date = $date->text();
                                // $date = date("Y-m-d", strtotime($date));

                                $payload = [
                                    'url' => $detail_url,
                                    'campaign_id' => $this->campaign->_id,
                                    'title' => $title,
                                    'content' => $content,
                                    'content_origin' => $content_origin,
                                    'scores' => $this->sentiment->scores($content),
                                    'sentiment' => $this->sentiment->decision($content),
                                    'score' => $this->sentiment->score($content),
                                    // 'words' => $this->sentiment->words($content),
                                    'date' => $date,
                                    'source' => $this->source->_id,
                                    'source_name' => $this->source->title,
                                ];
                                $persist = new Persistence();

                                $check_post = Article::where('url', '=', $detail_url)
                                    ->where('campaign_id', '=', $this->campaign->_id)
                                    ->first();

                                if (is_null($check_post)) {
                                    $persist->prepareFlat( $payload, new Article)->save();
                                }
                            }
                        }
                        DB::commit();
                    }
                });

                if ($posts->count() == 0) {
                    break;
                }

            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return $e->getMessage();
            }
        }

    }


    public function getDataGoogle()
    {
        for ($i=0; $i < $this->max_page; $i++) {
            $url_pages = sprintf($this->source->el_url, urlencode($this->campaign->keyword), $i*10);
            $client = new \Goutte\Client();
            $crawler = $client->request('GET', $url_pages);
            \Log::info("Scrap URL: " . $url_pages);
            $posts = $crawler->filter($this->source->el_list);

            $posts->each(function($article) use ($client) {
                if ($article->count() > 0) {
                    if ($this->source->el_url_prefix == 'on') {
                        $detail_url = $this->source->site . $article->filter("h3 a")->attr("href");
                    } else {
                        $detail_url = $article->filter("h3 a")->attr("href");
                    }
                    if (count(explode('/search?q=', $detail_url)) > 1) {
                        return;
                    }
                    $detail_url = explode('&', str_replace('/url?q=', '', $detail_url))[0];
                    if (count(explode('/tag', $detail_url)) > 1 OR count(explode('/indeks', $detail_url)) > 1) {
                        return;
                    }

                    DB::beginTransaction();

                    $check_post = Article::where('url', '=', $detail_url)
                        ->where('campaign_id', '=', $this->campaign->_id)
                        ->first();

                    if ($check_post == null) {
                        $get_article = $client->request('GET', $detail_url );
                        $title = $get_article->filter($this->source->el_title);
                        $content = $get_article->filter($this->source->el_description);
                        $date = $get_article->filter($this->source->el_date);
                        if ($title->count() > 0 && $content->count() > 0 && $date->count() > 0) {
                            \Log::info("#".$this->campaign->_id.", get article: $detail_url");
                            $title = $title->text();
                            $content = $content->text();
                            $content = trim_content($content);
                            $date = $date->text();
                            if (strpos($content, $this->campaign->keyword) === false) {
                                return;
                            }
                            // $date = date("Y-m-d", strtotime($date));

                            $payload = [
                                'url' => $detail_url,
                                'campaign_id' => $this->campaign->_id,
                                'title' => $title,
                                'content' => $content,
                                'scores' => $this->sentiment->scores($content),
                                'sentiment' => $this->sentiment->decision($content),
                                'score' => $this->sentiment->score($content),
                                'words' => $this->sentiment->words($content),
                                'date' => $date,
                                'source' => $this->source->_id,
                                'source_name' => $this->source->title,
                            ];
                            $persist = new Persistence();
                            $persist->prepareFlat( $payload, new Article)->save();
                        }
                    }
                    DB::commit();
                }
            });
            if ($posts->count() == 0) {
                break;
            }
        }

    }
}
