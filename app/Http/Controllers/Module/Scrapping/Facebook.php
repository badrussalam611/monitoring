<?php

namespace App\Http\Controllers\Module\Scrapping;

use Antoineaugusti\LaravelSentimentAnalysis\SentimentAnalysis;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Infrastructure\Persistence\Persistence;
use App\Model\Article;
use App\Model\Campaign;
use App\Model\Post;
use App\Model\Source;
use Illuminate\Http\Request;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;

class Facebook extends Controller
{
    protected $source;
    protected $campaign;
    protected $sentiment;
    protected $limit = 100;
    protected $fb;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct($limit = null, LaravelFacebookSdk $fb)
    {
        $this->sentiment = new SentimentAnalysis();
        if ($limit !== null) {
            $this->limit = $limit;
        }
        $this->fb = $fb;
        $this->fb->setDefaultAccessToken( get_option('fb_token') );
    }

    public function init()
    {
        $campaigns = Campaign::where('status', '=', 'running')->get();
        foreach ($campaigns as $campaign) {
            \Log::info("========================================");
            \Log::info("Start Campaign Facebook: $campaign->title");
            $this->initSource($campaign);
            \Log::info("End of Campaign Facebook: $campaign->title");
            \Log::info("========================================");
        }
    }

    public function initSource($campaign)
    {
        $this->campaign = $campaign;
        foreach ($campaign->sources as $source) {
            $this->source = Source::where('_id', '=', $source)->first();
            if ($this->source && in_array($this->source->type, ['facebook'])) {
                if (isset($this->campaign->facebook) && $this->campaign->facebook !== '') {
                    \Log::info("Start Facebook Get Posts: ");
                    $this->getPosts( $this->campaign->facebook );
                    $this->getPostComments( $this->campaign->_id );
                    \Log::info("End of Facebook Get Posts: ");
                }
            }
        }
    }

    public function getPosts($fbuid)
    {
        try {
            $response = $this->fb->get("/{$fbuid}?fields=posts.limit({$this->limit})");
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            \Log::error($e->getMessage());
        }

        // Convert the response to a `Facebook/GraphNodes/getGraphPage` collection
        $facebook_page = $response->getGraphPage();
        $fbposts = $facebook_page['posts'];
        foreach ($fbposts as $post) {
            $check_post = Post::where('post_id', '=', $post['id'])->first();
            if ($check_post == null) {
                $detail_url = "https://www.facebook.com/{$post['id']}";
                \Log::info("Facebook POST URL: {$detail_url}");
                $payload = [
                    'post_id' => $post['id'],
                    'url' => $detail_url,
                    'message' => (!empty($post['message']))? trim_content($post['message']):'',
                    'date' => $post['created_time'],
                    'source' => 'facebook',
                    'campaign_id' => $this->campaign->_id
                ];
                $persist = new Persistence();
                $persist->prepareFlat( $payload, new Post)->save();
            }
        }
    }

    public function getPostComments($campaign_id)
    {
        $posts = Post::where('campaign_id', '=', $campaign_id)
            ->where('source', '=', 'facebook')
            ->get();
        foreach ($posts as $post) {
            try {
                $response = $this->fb->get("/{$post['post_id']}?fields=comments.limit({$this->limit})");
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                \Log::error($e->getMessage());
            }

            // Convert the response to a `Facebook/GraphNodes/getGraphPage` collection
            $facebook_detail = $response->getGraphNode();
            $fb_comments = (!empty($facebook_detail['comments']))? $facebook_detail['comments']:[];
            foreach ($fb_comments as $comment) {
                $content = (!empty($comment['message']))? $comment['message']:'';
                $content_trim = trim_content($content);
                $detail_url = "https://www.facebook.com/{$comment['id']}";

                $check_post = Article::where('url', '=', $detail_url)
                    ->where('campaign_id', '=', $this->campaign->_id)
                    ->first();

                if ($check_post == null) {
                    $payload = [
                        'url' => $detail_url,
                        'campaign_id' => $this->campaign->_id,
                        'title' => $content_trim,
                        'content' => $content,
                        'scores' => $this->sentiment->scores($content_trim),
                        'sentiment' => $this->sentiment->decision($content_trim),
                        'score' => $this->sentiment->score($content_trim),
                        'words' => $this->sentiment->words($content_trim),
                        'date' => $comment['created_time'],
                        'source' => $this->source->_id,
                        'source_name' => $this->source->title,
                        'post_id' => $post['post_id'],
                        'facebook' => [
                            'user_id' => $comment['from']['id'],
                            'user_name' => $comment['from']['name'],
                        ]
                    ];
                    $persist = new Persistence();
                    $persist->prepareFlat( $payload, new Article)->save();
                }

            }

        }
    }
}
