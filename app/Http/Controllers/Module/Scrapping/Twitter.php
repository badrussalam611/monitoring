<?php

namespace App\Http\Controllers\Module\Scrapping;

use Antoineaugusti\LaravelSentimentAnalysis\SentimentAnalysis;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Infrastructure\Persistence\Persistence;
use App\Model\Article;
use App\Model\Campaign;
use App\Model\Source;
use Illuminate\Http\Request;
use Thujohn\Twitter\Facades\Twitter as TTwitter;

class Twitter extends Controller
{
    protected $source;
    protected $limit = 500;
    protected $campaign;
    protected $sentiment;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct($limit = null)
    {
        $this->sentiment = new SentimentAnalysis();
        if ($limit !== null) {
            $this->limit = $limit;
        }
    }

    public function init()
    {
        $campaigns = Campaign::where('status', '=', 'running')->get();
        foreach ($campaigns as $campaign) {
            \Log::info("========================================");
            \Log::info("Start Campaign Twitter: $campaign->title");
            \Log::info("========================================");
            $this->initSource($campaign);
            $this->initDataTwitter($campaign);
            \Log::info("End of Campaign Twitter: $campaign->title");
            \Log::info("========================================");
            \Log::info("========================================");
        }
    }

    public function initSource($campaign)
    {
        $this->campaign = $campaign;
        foreach ($campaign->sources as $source) {
            $this->source = Source::where('_id', '=', $source)->first();
            if ($this->source && in_array($this->source->type, ['twitter'])) {
                // $this->max_page = 20;
                \Log::info("Start Twitter Search: " . $this->campaign->keyword);
                $this->searchTweets($this->campaign->keyword);
                if (isset($this->campaign->twitter) && $this->campaign->twitter !== '') {
                    \Log::info("Start Twitter Search Mention Account: @" . $this->campaign->twitter);
                    $this->searchTweets( "@" . $this->campaign->twitter );
                }
                \Log::info("End of Twitter Search: " . $this->campaign->keyword);
            }
        }
    }

    public function initDataTwitter($campaign)
    {
        /** Update data twitter */
        if (isset($campaign->twitter) && $campaign->twitter !== '') {
            $twitterAccount = TTwitter::getUsersLookup(['screen_name' => $campaign->twitter, 'include_entities' => 0]);

            if (count($twitterAccount) > 0) {
                $twitterAccount = $twitterAccount[0];
                $campaign->twitter_data = [
                    'id' => $twitterAccount->id,
                    'desc' => $twitterAccount->description,
                    'name' => $twitterAccount->name,
                    'friends' => $twitterAccount->friends_count,
                    'followers' => $twitterAccount->followers_count,
                ];
                $campaign->save();
            }
        }
    }

    public function searchTweets($keyword = null)
    {
        /** Initialize data */
        $contents = array();
        $max_id = null;
        for ($count = 100; $count <= $this->limit; $count += 100) {
            if (null !== $max_id && $max_id == '') {
                break;
            }
            $content = TTwitter::getSearch([
                'q' => $keyword, 
                'count' => 100, 
                'result_type' => 'recent', 
                'include_entities' => false,
                'max_id' => $max_id
            ]);

            // array_push($content->statuses, $contents);
            $contents = array_merge($contents, $content->statuses);
            // this indicates the last index of $content array
            if (count($content->statuses) > 1) {
                $max_id = $content->statuses[count($content->statuses) - 1]->id_str;
            }
            if (count($content->statuses) > 0) {
                $last_tweet = end($content->statuses);
                $max_id = $last_tweet->id_str;
            } else $max_id = null;
        }

        /** Insert into databases */
        foreach (array_reverse($contents) as $tweet) {
            
            $check_post = Article::where('url', '=', TTwitter::linkTweet($tweet))
                ->where('campaign_id', '=', $this->campaign->_id)
                ->first();

            if ($check_post == null) {
                \Log::info("#".$this->campaign->_id.", get tweet: " . TTwitter::linkTweet($tweet));
                $content_trim = trim_content($tweet->text);
                $payload = [
                    'url' => TTwitter::linkTweet($tweet),
                    'campaign_id' => $this->campaign->_id,
                    'title' => $content_trim,
                    'content' => $tweet->text,
                    'scores' => $this->sentiment->scores($content_trim),
                    'sentiment' => $this->sentiment->decision($content_trim),
                    'score' => $this->sentiment->score($content_trim),
                    // 'words' => $this->sentiment->words($content_trim),
                    'date' => $tweet->created_at,
                    'source' => $this->source->_id,
                    'source_name' => $this->source->title,
                    'twitter' => $tweet,
                ];

                $persist = new Persistence();
                $persist->prepareFlat( $payload, new Article)->save();
            }
        }
    }
}
