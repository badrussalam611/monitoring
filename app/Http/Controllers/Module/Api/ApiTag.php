<?php

namespace App\Http\Controllers\Module\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Infrastructure\Persistence\Persistence;
use App\Model\Article;
use App\Model\Campaign;
use App\Model\Source;
use App\Model\TagCloud;
use Illuminate\Http\Request;
use App\Http\Controllers\Module\Api\ApiArticle;

class ApiTag extends Controller
{
    public function index($id = null, Request $request)
    {
        if ($id == null) {
            return response()->json("What are you doing dud?!", 404);
        }
        $s = '';

        /** Get Tag from DB */
        $persist = new Persistence();
        $tagclouds = TagCloud::where('campaign_id', '=', $id);
        if ($request->query('s') !== null) {
            $s = trim($request->query('s'));
            $tagclouds = $tagclouds->where('focus', '=', $s);
        }

        /** Initiate payload */
        $payload = [
            'campaign_id' => $id,
            'focus' => $s,
            'data' => []
        ];

        $tagclouds = $tagclouds->first();
        if ($tagclouds == null) {
            $tagclouds = new TagCloud();
            $persist->prepareFlat( $payload, $tagclouds)->save();
        }

        $apiArticle = new ApiArticle();
        $articles = $apiArticle->getArticles($id, $request);
        $dataArr = $tagclouds->data;

        foreach ($articles as $article) {
            $hasTags = [];
            if (isset($article->hasTags)) {
                $hasTags = $article->hasTags;
            }

            if (!in_array($tagclouds->_id, $hasTags)) {
                $words = '';
                $words = clean(trim_content($article->title . ' ' . $article->content));
                $wordsArr = explode(' ', $words);
                foreach ($wordsArr as $item) {
                    if (check_exclude(strtolower($item))) continue;

                    if (isset($dataArr[$item])) {
                        $dataArr[$item] += 1;
                    } else {
                        $dataArr[$item] = 1;
                    }
                }
                $hasTags[] = $tagclouds->_id;
                $article->hasTags = $hasTags;
                $article->save();
            }
        }
        arsort($dataArr);
        $payload['data'] = $dataArr;
        $persist->prepareFlat( $payload, $tagclouds)->save();
        $tmpArr = array();
        foreach ($dataArr as $key => $value) {
            $tmpArr[] = [
                'text' => $key,
                'weight' => $value,
            ];
        }
        return response()->json([
            'data' => $tmpArr,
            'count' => count($dataArr),
        ], 200);
    }
}
