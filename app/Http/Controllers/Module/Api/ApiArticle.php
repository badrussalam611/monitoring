<?php

namespace App\Http\Controllers\Module\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Infrastructure\Persistence\Persistence;
use App\Model\Article;
use App\Model\Campaign;
use App\Model\Source;
use Illuminate\Http\Request;

class ApiArticle extends Controller
{
    public function index($id = null)
    {
        global $i;
        if ($id == null) {
            return response()->json("What are you doing dud?!", 404);
        }
        $articles = Article::where('campaign_id', '=', $id)->get(['title', 'score', 'sentiment', 'source', 'source_name', 'url']);
        $i = 1;
        $articles->map(function ($article) {
            global $i;
            $article->no = $i;
            $article->title .= ' <a href="'.$article->url.'" target="_blank" style="color: #0cc2aa;"><i class="fa fa-external-link"></i></a>';
            $article->sentiment = '<b class="label success '. strtolower($article->sentiment) . '">'.$article->score * 100 .'%</b> '. $article->sentiment;
            $i++;
        });
        return response()->json([
            'aaData' => $articles
        ], 200);
    }

    public function getArticleByMedia($id = null, Request $request)
    {
        if ($id == null) {
            return response()->json("What are you doing dud?!", 404);
        }
        $articles = Article::where('campaign_id', '=', $id);

        /** For Search with variable "s" */
        if ($request->query('s') !== null) {
            $s = trim($request->query('s'));
            $articles = $articles->where(function($query) use($s) {
                $query->Where('title', 'LIKE', '%' . $s . '%');
                $query->orWhere('content', 'LIKE', '%' . $s . '%');
            });
        }
        $articles = $articles->get(['title', 'score', 'scores', 'sentiment', 'source', 'source_name', 'url']);
        $groupedMedia = $articles;
        $groupedMedia = $groupedMedia->groupBy('source');
        $data = [];
        $i = 1;

        foreach ($groupedMedia as $key => $medias) {
            $positive = $negative = $neutral = 0;
            foreach ($medias as $item) {
                $positive += $item->scores['positive'];
                $negative += $item->scores['negative'];
                $neutral += $item->scores['neutral'];
            }
            $total = count($medias);
            $sentiment = "positive";
            if ($positive < $negative && $neutral < $negative) {
                $sentiment = "negative";
            } elseif ($positive < $neutral && $negative < $neutral) {
                $sentiment = "neutral";
            }
            $data[] = [
                'no' => $i,
                'title' => Source::where('_id', '=', $key)->first()->title,
                'positive' => ($positive > 0)? round(($positive/$total) * 100, 2) . '%':'0%',
                'negative' => ($negative > 0)? round(($negative/$total) * 100, 2) . '%':'0%',
                'neutral' => ($neutral > 0)? round(($neutral/$total) * 100, 2) . '%':'0%',
                'sentiment' => '<b class="label success '. strtolower($sentiment) . '">'.$sentiment .'</b>',
                'total' => $total,
            ];
            $i++;
        }

        return response()->json([
            'aaData' => $data
        ], 200);
    }

    public function getArticleByFocus($id = null, Request $request)
    {
        if ($id == null OR $request->query('s') == null) {
            return response()->json("What are you doing dud?!", 404);
        }

        /** Get Articles */
        $articles = $this->getArticles($id, $request);
        $s = $request->query('s');

        $positive = $negative = $neutral = 0;
        foreach ($articles as $article) {
            $positive += $article->scores['positive'];
            $negative += $article->scores['negative'];
            $neutral += $article->scores['neutral'];
        }
        $sentiment = "positive";
        if ($positive < $negative && $neutral < $negative) {
            $sentiment = "negative";
        } elseif ($positive < $neutral && $negative < $neutral) {
            $sentiment = "neutral";
        }
        $total = count($articles);
        $data = [
            'title' => $s,
            'positive' => ($positive > 0)? round(($positive/$total) * 100, 2):'0',
            'negative' => ($negative > 0)? round(($negative/$total) * 100, 2):'0',
            'neutral' => ($neutral > 0)? round(($neutral/$total) * 100, 2):'0',
            'sentiment' => $sentiment,
            'total' => $total,
        ];

        return response()->json([
            'data' => $data,
            'chart' => "[{data: ".$data['positive'].", label: 'Positive'},{data: ".$data['negative'].", label: 'Negative'},{data: ".$data['neutral'].", label: 'Neutral'}]",
        ]);
    }


    public function getArticles($id = null, $request)
    {
        if ($id == null) {
            return response()->json("What are you doing dud?!", 404);
        }
        $articles = Article::where('campaign_id', '=', $id);

        /** For Search with variable "s" */
        $s = trim($request->query('s'));
        if ($s !== null) {
            $articles = $articles->where(function($query) use($s) {
                $query->Where('title', 'LIKE', '%' . $s . '%');
                $query->orWhere('content', 'LIKE', '%' . $s . '%');
            });
        }
        $articles = $articles->get();

        return $articles;
    }
}
