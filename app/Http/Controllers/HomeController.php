<?php

namespace App\Http\Controllers;

use App\Model\Article;
use App\Model\Campaign;
use App\Model\Source;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['campaigns'] = Campaign::all();
        $this->data['articles'] = Article::all();
        $this->data['sources'] = Source::all();
        return view('pages.dashboard', $this->data);
    }
}
