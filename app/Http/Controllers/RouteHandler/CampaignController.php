<?php

namespace App\Http\Controllers\RouteHandler;

use Alert;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Infrastructure\Persistence\Persistence;
use App\Http\Controllers\Module\Scrapping\News;
use App\Http\Controllers\Module\Scrapping\Twitter;
use App\Jobs\ScrapFirstData;
use App\Model\Article;
use App\Model\Campaign;
use App\Model\Post;
use App\Model\Source;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CampaignController extends Controller
{
    /** initiate data */
    protected $data = array();

    /**
     * Index page campaigns
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['campaigns'] = Campaign::all();
        return view('campaigns.index', $this->data);
    }

    /**
     * Display form create campaign
     * @return [type] [description]
     */
    public function create()
    {
        $this->data['sources'] = Source::all();
        return view('campaigns.create', $this->data);
    }

    /**
     * Save create campaign
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request, $id = null)
    {
        $persist = new Persistence();
        $payload = [
            "title" => $request->get('title'),
            "keyword" => $request->get('keyword'),
            "status" => $request->get('status'),
            "twitter" => $request->get('twitter'),
            "instagram" => $request->get('instagram'),
            "facebook" => $request->get('facebook'),
            "sources" => $request->get('sources'),
            "focus" => $request->get('focus'),
        ];

        if ($id !== null) {
            $source = Campaign::where('_id', '=', $id)->first();
            if ($persist->prepareFlat( $payload, $source)->save()) {
                Alert::success('Campaign successfully edited!');
                return redirect()->back();
            } else {
                Alert::danger('Campaign cannot be save, please check your field!');
                return redirect()->back();
            }
        } else {

            $persist->prepareFlat( $payload, new Campaign)->save();
            Alert::success('Campaign successfully created!');
            return redirect()->route('campaigns');
        }
    }

    public function delete($id)
    {
        /** Delete campaign */
        Campaign::where('_id', '=', $id)->delete();

        /** Delete data article & post */
        Article::where('campaign_id', '=', $id)->delete();
        Post::where('campaign_id', '=', $id)->delete();

        Alert::success('Campaign successfully deleted!');
        return redirect()->back();
    }

    public function viewReport($id = null)
    {
        if ($id == null) {
            return redirect()->route('campaign');
        }

        $campaign = Campaign::where('_id', '=', $id)->first();
        if ($campaign) {
            $articles = Article::where('campaign_id', '=', $campaign->_id)
                ->where('source', '!=', 'Twitter')
                // ->orderBy('date')
                ->get();
                
            $scores = array('positive' => 0,'negative' => 0,'neutral' => 0,'total' => 0,);

            if (count($articles) > 0) {

                /** Calculated all scores */
                foreach ($articles as $item) {
                    $scores['positive'] += $item->scores['positive'];
                    $scores['negative'] += $item->scores['negative'];
                    $scores['neutral'] += $item->scores['neutral'];
                    $scores['total'] += $item->scores['positive'] + $item->scores['negative'] + $item->scores['neutral'];
                }
                $this->data['persent_positive'] = ($scores['positive']/$scores['total'])*100;
                $this->data['persent_negative'] = ($scores['negative']/$scores['total'])*100;
                $this->data['persent_neutral'] = ($scores['neutral']/$scores['total'])*100;
            } else {
                $this->data['persent_positive'] = 0;
                $this->data['persent_negative'] = 0;
            }
            
            $this->data['scores'] = $scores;
            $this->data['articles'] = $articles;
            $this->data['campaign'] = $campaign;


            return view('campaigns.view-report', $this->data);
        }

        abort('404');
    }

    /**
     * Display form create campaign
     * @return [type] [description]
     */
    public function editForm($id)
    {
        $this->data['campaign'] = Campaign::where('_id', '=', $id)->first();
        $this->data['sources'] = Source::all();
        return view('campaigns.edit', $this->data);
    }

    public function doScrapManual($id = null)
    {
        if ($id == null) {
            return redirect()->route('campaign');
        }
        $campaign = Campaign::where('_id', '=', $id)->first();

        if ($campaign) {
            $job = (new ScrapFirstData($campaign, 50));
            dispatch($job);
            Alert::success('Campaign will start scrap on background!');
            return redirect()->back();
        }

        Alert::danger('Campaign wrong, cannot start scrap manual!');
        return redirect()->back();
    }

    public function export($id = null)
    {
        $campaign = Campaign::where('_id', '=', $id)->first();
        $articles = Article::where('campaign_id', '=', $id)
            ->orderBy('date');

        /** NEWS SECTION */
        $articlesNews = Article::where('campaign_id', '=', $id)
            ->whereNotIn('source_name', ['Twitter', 'Facebook', 'Instagram'])
            ->get();
        $dataArticles = [['id', 'title', 'content', 'url', 'sentiment', 'score positive', 'score negative', 'score neutral', 'source', 'created at']];
        foreach ($articlesNews as $item) {
            $dataArticles[] = [
                $item->_id,
                $item->title,
                $item->content,
                $item->url,
                $item->sentiment,
                $item->scores['positive'],
                $item->scores['negative'],
                $item->scores['neutral'],
                $item->source_name,
                $item->date
            ];
        }
        /** END OF NEWS SECTION */

        /** TWITTER SECTION */
        $dataTwitter = [['no', 'account', 'url', 'followers', 'following']];
        $totalTwitter = ['followers' => 0,'following' => 0];
        $articlesTweet = Article::where('campaign_id', '=', $id)
            ->whereIn('source_name', ['Twitter'])
            ->get();
            
        $dataTweets = [['id', 'content', 'url', 'sentiment', 'score positive', 'score negative', 'score neutral', 'source', 'user', 'user link', 'created at']];
        $i = 1;
        foreach ($articlesTweet as $item) {
            $twitter = $item->twitter;
            $twitterUser = $twitter['user'];
            $dataTweets[] = [
                $item->_id,
                $item->content,
                $item->url,
                $item->sentiment,
                $item->scores['positive'],
                $item->scores['negative'],
                $item->scores['neutral'],
                $item->source_name,
                $twitterUser['screen_name'],
                'https://twitter.com/' . $twitterUser['screen_name'],
                $item->date,
            ];
            if (empty($dataTwitter[$twitterUser['screen_name']])) {
                $dataTwitter[$twitterUser['screen_name']] = [
                    $i++,
                    $twitterUser['screen_name'],
                    'https://twitter.com/'.$twitterUser['screen_name'],
                    (isset($twitterUser['followers_count']))? $twitterUser['followers_count']:'0',
                    (isset($twitterUser['friends_count']))? $twitterUser['friends_count']:'0',
                ];
                $totalTwitter['followers'] += (isset($twitterUser['followers_count']))? $twitterUser['followers_count']:0;
                $totalTwitter['following'] += (isset($twitterUser['friends_count']))? $twitterUser['friends_count']:0;
            }
        }
        /** END OF TWITTER SECTION */


        /** FACEBOOK SECTION */
        // $dataFacebook = [['no', 'account', 'url', 'name']];
        // $articlesPost = $articles->whereIn('source_name', ['Facebook']);
        // $dataFbPosts = [['id', 'content', 'url', 'sentiment', 'score positive', 'score negative', 'score neutral', 'source', 'post', 'user name', 'user link', 'created at']];
        // $i = 1;
        // foreach ($articlesPost as $item) {
        //     $date = (!empty($item->date['date']))? $item->date['date']:$item->date;
        //     $dataFbPosts[] = [
        //         $item->_id,
        //         $item->content,
        //         $item->url,
        //         $item->sentiment,
        //         $item->scores['positive'],
        //         $item->scores['negative'],
        //         $item->scores['neutral'],
        //         $item->source_name,
        //         'https://www.facebook.com/' . $item->post_id,
        //         $item->facebook['user_name'],
        //         'https://www.facebook.com/'.$item->facebook['user_id'],
        //         $date
        //     ];
        //     $facebook = $item->facebook;
        //     if (empty($dataFacebook[$facebook['user_id']])) {
        //         $dataFacebook[$facebook['user_id']] = [
        //             $i++,
        //             $facebook['user_id'],
        //             'https://www.facebook.com/'.$facebook['user_id'],
        //             $facebook['user_name'],
        //         ];
        //     }
        // }
        /** END OF FACEBOOK SECTION */

        /** INSTAGRAM SECTION */
        // $dataInsta = [['no', 'account', 'url', 'name', 'followers', 'following']];
        // $totalInsta = ['followers' => 0,'following' => 0];
        // $articlesInsta = $articles->whereIn('source_name', ['Instagram']);
        // $dataInstas = [['id', 'content', 'url', 'sentiment', 'score positive', 'score negative', 'score neutral', 'source', 'post id', 'user', 'user link', 'created at']];
        // $i = 1;
        // foreach ($articlesInsta as $item) {
        //     $dataInstas[] = [
        //         $item->_id,
        //         $item->content,
        //         (!empty($item->instagram['url']))? $item->instagram['url']: $item->posturl(),
        //         $item->sentiment,
        //         $item->scores['positive'],
        //         $item->scores['negative'],
        //         $item->scores['neutral'],
        //         $item->source_name,
        //         $item->post_id,
        //         $item->instagram['user_name'],
        //         'https://instagram.com/' . $item->instagram['user_name'],
        //         $item->date,
        //     ];
        //     $instagram = $item->instagram;
        //     if (empty($dataInsta[$instagram['user_id']])) {
        //         $dataInsta[$instagram['user_id']] = [
        //             $i++,
        //             $instagram['user_name'],
        //             'https://instagram.com/'.$instagram['user_name'],
        //             $instagram['user_fullname'],
        //             (isset($instagram['followers']))? $instagram['followers']:'0',
        //             (isset($instagram['following']))? $instagram['following']:'0',
        //         ];
        //         $totalInsta['followers'] += (isset($instagram['followers']))? $instagram['followers']:0;
        //         $totalInsta['following'] += (isset($instagram['following']))? $instagram['following']:0;
        //     }
        // }
        /** END OF INSTAGRAM SECTION */

        
        Excel::create('Campaign Sentiment - ' . $campaign->title, function($excel) use($dataArticles, $dataTweets, $dataTwitter, $totalTwitter) {
            $excel->sheet('Articles', function($sheet) use($dataArticles) {
                $sheet->fromArray($dataArticles);
            });
            $excel->sheet('All Tweets', function($sheet) use($dataTweets) {
                $sheet->fromArray($dataTweets);
            });
            $excel->sheet('Account Twitter', function($sheet) use($dataTwitter, $totalTwitter) {
                $sheet->fromArray($dataTwitter);
                $sheet->appendRow(['', 'TOTAL', '', $totalTwitter['followers'], $totalTwitter['following']]);
            });
            // $excel->sheet('FB Comments', function($sheet) use($dataFbPosts) {
            //     $sheet->fromArray($dataFbPosts);
            // });
            // $excel->sheet('FB User', function($sheet) use($dataFacebook) {
            //     $sheet->fromArray($dataFacebook);
            // });
            // $excel->sheet('Instagram Comments', function($sheet) use($dataInstas) {
            //     $sheet->fromArray($dataInstas);
            // });
            // $excel->sheet('Account Instagram', function($sheet) use($dataInsta, $totalInsta) {
            //     $sheet->fromArray($dataInsta);
            //     $sheet->appendRow(['', 'TOTAL', '', '', $totalInsta['followers'], $totalInsta['following']]);
            // });
        })->export('xlsx');

    }
}
