<?php

namespace App\Http\Controllers\RouteHandler;

use Alert;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Infrastructure\Persistence\Persistence;
use App\Model\Setting;
use App\Model\Source;
use Carbon\Carbon;
use Illuminate\Http\Request;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk as FB;

class SosmedController extends Controller
{
    public function fbLogin(FB $fb)
    {
        // Send an array of permissions to request
        $login_url = $fb->getLoginUrl();
        return redirect($login_url);
    }

    public function fbCallback(FB $fb)
    {
        // Obtain an access token.
        try {
            $token = $fb->getAccessTokenFromRedirect();
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }

        // Access token will be null if the user denied the request
        // or if someone just hit this URL outside of the OAuth flow.
        if (! $token) {
            // Get the redirect helper
            $helper = $fb->getRedirectLoginHelper();

            if (! $helper->getError()) {
                abort(403, 'Unauthorized action.');
            }

            // User denied the request
            dd(
                $helper->getError(),
                $helper->getErrorCode(),
                $helper->getErrorReason(),
                $helper->getErrorDescription()
            );
        }

        if (! $token->isLongLived()) {
            // OAuth 2.0 client handler
            $oauth_client = $fb->getOAuth2Client();

            // Extend the access token.
            try {
                $token = $oauth_client->getLongLivedAccessToken($token);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                dd($e->getMessage());
            }
        }

        $fb->setDefaultAccessToken($token);

        Setting::updateOrCreate(['key' => 'fb_token'], ['value' => (string) $token]);
        Setting::updateOrCreate(['key' => 'fb_token_expired'], ['value' => (string) Carbon::now()->addMonths(2)]);

        Alert::success('Facebook Token already updated!');
        return redirect()->back();
    }
}
