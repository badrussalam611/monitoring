<?php

namespace App\Http\Controllers\RouteHandler;

use Alert;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Infrastructure\Persistence\Persistence;
use App\Model\Source;
use Illuminate\Http\Request;

class SourceController extends Controller
{
    /** initiate data */
    protected $data = array();

    /**
     * Index page campaigns
     * @return [type] [description]
     */
    public function index()
    {
        $this->data['sources'] = Source::all();
        return view('sources.index', $this->data);
    }

    /**
     * Display form create campaign
     * @return [type] [description]
     */
    public function create()
    {
        return view('sources.create');
    }

    /**
     * Save create campaign
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request, $id = null)
    {
        $persist = new Persistence();
        $payload = [
            "title" => $request->get('title'),
            "site" => $request->get('site'),
            "el_list" => $request->get('el_list'),
            "el_url_prefix" => $request->get('el_url_prefix'),
            "el_url" => $request->get('el_url'),
            "el_title" => $request->get('el_title'),
            "el_description" => $request->get('el_description'),
            "el_date" => $request->get('el_date'),
            "type" => $request->get('type'),
        ];

        if ($id !== null) {
            $source = Source::where('_id', '=', $id)->first();
            if ($persist->prepareFlat( $payload, $source)->save()) {
                Alert::success('Source successfully edited!');
                return redirect()->route('sources');
                // return redirect()->back();
            } else {
                Alert::danger('Source cannot be save, please check your field!');
                return redirect()->back();
            }
        } else {
            $persist = new Persistence();

            $persist->prepareFlat( $payload, new Source)->save();
            Alert::success('Source successfully created!');
            return redirect()->route('sources');
        }
    }

    /**
     * Display form create campaign
     * @return [type] [description]
     */
    public function editForm($id)
    {
        $this->data['source'] = Source::where('_id', '=', $id)->first();
        return view('sources.edit', $this->data);
    }

    public function clone($id = null)
    {
        $source = Source::where('_id', '=', $id)->first();

        if ($source) {
            $payload = [
                "title" => $source->title . ' - Copy',
                "site" => $source->site,
                "el_list" => $source->el_list,
                "el_url_prefix" => $source->el_url_prefix,
                "el_url" => $source->el_url,
                "el_title" => $source->el_title,
                "el_description" => $source->el_description,
                "el_date" => $source->el_date,
                "type" => $source->type,
            ];
            $persist = new Persistence();
            $persist->prepareFlat( $payload, new Source)->save();
            Alert::success('Source successfully duplicated!');
            return redirect()->back();
        }
        Alert::warning('Source cannot duplicate!');
        return redirect()->back();
    }
}
