<?php

namespace App\Http\Controllers\RouteHandler;

use Alert;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Infrastructure\Persistence\Persistence;
use App\Model\Setting;
use App\Model\Source;
use Carbon\Carbon;
use Illuminate\Http\Request;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk as FB;

class SettingController extends Controller
{
    public function index()
    {
        return view('pages.settings');
    }
}
