<?php

namespace App\Model;

use App\Model\Post;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Article extends Eloquent
{
    
    protected $connection = 'mongodb';
    protected $collection = 'articles';

    public function Sources()
    {
        return $this->has('App\Model\Source', '_id', 'source');
    }

    public function Posturl()
    {
        $post = Post::where('post_id', '=', $this->post_id)->first();
        if ($post) {
            return $post->url;
        }
        return null;
    }
}
