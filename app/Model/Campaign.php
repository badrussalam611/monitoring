<?php

namespace App\Model;

use App\Model\CampaignActivity;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Campaign extends Eloquent
{

    protected $connection = 'mongodb';
    protected $collection = 'campaigns';

    /**
     * Product Has Many Meta
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Article()
    {
        return $this->hasMany('App\Model\Article', 'campaign_id', '_id');
    }

    /**
     * Product Has Many Meta
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function TagCloud()
    {
        return $this->hasMany('App\Model\TagCloud', 'campaign_id', '_id');
    }

    /**
     * Product Has Many Meta
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Activity()
    {
        return $this->hasMany('App\Model\CampaignActivity', 'campaign_id', '_id');
    }

    public function isInqueue($source_id = null)
    {
        $activity = CampaignActivity::where('campaign_id', $this->_id)
            ->where('source_id', $source_id)
            ->latest()
            ->first();

        if (!is_null($activity) && $activity->status == 'inqueue') {
            return true;
        }

        return false;
    }

    public function setActivity($source_id = null, $status = 'inqueue')
    {
        $activity = new CampaignActivity;
        $activity->campaign_id = $this->_id;
        $activity->source_id = $source_id;
        $activity->status = $status;
        $activity->save();
    }
}
