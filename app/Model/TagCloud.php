<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class TagCloud extends Eloquent
{
    
    protected $connection = 'mongodb';
    protected $collection = 'tagcloud';


    /**
     * Product Has Many Meta
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Campaign()
    {
        return $this->has('App\Model\Campaign', '_id', 'campaign_id');
    }
}
