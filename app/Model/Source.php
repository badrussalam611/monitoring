<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Source extends Eloquent
{
    
    protected $connection = 'mongodb';
    protected $collection = 'sources';

    /**
     * Product Has Many Meta
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Article()
    {
        return $this->hasMany('App\Model\Article', 'source', '_id');
    }
}
