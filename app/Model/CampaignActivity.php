<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CampaignActivity extends Eloquent
{

    protected $connection = 'mongodb';
    protected $collection = 'campaigns_activities';

    /**
     * Product Has Many Meta
     * @return \Illuminate\Database\Eloquent\Relations\Has
     */
    public function Campaign()
    {
        return $this->has('App\Model\Campaign', '_id', 'campaign_id');
    }
}
