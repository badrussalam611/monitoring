<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class User extends Eloquent
{

    protected $connection = 'mongodb';
    protected $collection = 'sources';

    public function run()
{
    User::factory()
            ->count(50)
            ->hasPosts(1)
            ->create();
}



}