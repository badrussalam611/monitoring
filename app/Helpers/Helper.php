<?php

/**
 * trim_kontent description
 * @param  [type] $konten [description]
 * @return [type]         [description]
 */
function trim_content($konten) {

    $konten = trim($konten);
    $konten = str_replace("\r\n", " ", $konten);
    $konten = str_replace("\n", " ", $konten);
    $konten = str_replace(".", "", $konten);
    $konten = str_replace("  ", " ", $konten);
    $konten = str_replace('--', '-', $konten);
    $konten = strtolower($konten);
    $konten = ConvertToUTF8($konten);

    return $konten;
}

function clean($konten) {
    $konten = str_replace(' ', '-', $konten);
    $konten = preg_replace('/[^A-Za-z0-9\-]/', '', $konten);
    $konten = str_replace('-', ' ', $konten);

    return $konten;
}

function num_format($nominal) {
    return number_format( intval($nominal), 0 , '' , '.' );
}

function ConvertToUTF8($text) {
    $encoding = mb_detect_encoding($text, mb_detect_order(), false);
    if ($encoding == "UTF-8") {
        $text = mb_convert_encoding($text, 'UTF-8', 'UTF-8');    
    }
    $out = iconv(mb_detect_encoding($text, mb_detect_order(), false), "UTF-8//IGNORE", $text);
    return $out;
}


function check_exclude($item) {
    $excludeVar = config('sentimen.excludetags');

    if (in_array($item, $excludeVar))
        return true;
    if (strpos($item, 'http') !== false)
        return true;
    if (strlen($item) <= 1)
        return true;

    return false;
}


/**
 * Get setting option by key
 * @param  string $key
 * @return string
 */
function get_option($key) {
    $token = App\Model\Setting::where('key', '=', $key)->first();
    if ($token) {
        return $token->value;
    }
    return false;
}




/**
 * Get containt between string
 *
 * @param string $str
 * @param string $from
 * @param string $to
 * @return string
 */
function get_string_between($str, $from, $to)
{
    $sub = substr( $str, strpos($str, $from) + strlen($from), strlen($str) );

    return substr ($sub, 0, strpos($sub,$to) );
}