<?php

namespace App\Console;

use App\Model\Campaign;
use App\Jobs\ScrapFirstData;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function() {
            $campaigns = Campaign::where('status', '=', 'running')->get();
            foreach ($campaigns as $campaign) {
                \Log::info("Schedule Campaign: {$campaign->title} Start!");
                dispatch( (new ScrapFirstData($campaign, 5)) );
            }
        })->cron('*/30 * * * * *');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
