<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Controllers\Module\Scrapping\News;

class ScrapNews implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $max_page;
    protected $campaign;
    protected $source;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($campaign, $source, $max_page)
    {
        $this->campaign = $campaign;
        $this->source   = $source;
        $this->max_page = $max_page;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $news = new News($this->max_page);
        $news->initSource($this->campaign, $this->source);
    }
}
