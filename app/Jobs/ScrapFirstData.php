<?php

namespace App\Jobs;

use App\Jobs\ScrapNews;
use App\Http\Controllers\Module\Scrapping\Facebook;
use App\Http\Controllers\Module\Scrapping\Instagram;
use App\Http\Controllers\Module\Scrapping\News;
use App\Http\Controllers\Module\Scrapping\Twitter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ScrapFirstData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $campaign;
    protected $max_page;
    public $timeout = 21600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($campaign, $max_page = 5)
    {
        $this->campaign = $campaign;
        $this->max_page = $max_page;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    // public function handle(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb)
    public function handle()
    {
        // $facebook = new Facebook(1000, $fb);
        // $instagram = new Instagram(1000);
        \Log::info("========================================");
        \Log::info("Start Campaign Queue: " . $this->campaign->title);
        foreach ($this->campaign->sources as $source) {
            dispatch( (new ScrapNews($this->campaign, $source, $this->max_page)) );
        }


        $twitter = new Twitter(1000);
        $twitterData = $twitter->initSource($this->campaign);
        $twitterData = $twitter->initDataTwitter($this->campaign);
        // $facebookData = $facebook->initSource($this->campaign);
        // $instagramData = $instagram->initSource($this->campaign);
        \Log::info("End of Campaign Queue: " . $this->campaign->title);
        \Log::info("========================================");
    }
}
